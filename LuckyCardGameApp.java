import java.util.Scanner;
public class LuckyCardGameApp{
	
	public static void main(String[] args)
	{
	Scanner input = new Scanner(System.in);
	Deck deck = new Deck();
	deck.shuffle();
	
	System.out.println("How many cards would you like to remove?");
	int amountOfCardsBeingRemoved = Integer.parseInt(input.nextLine());
	boolean validAmount = true ;
	
	while(validAmount){
		if(amountOfCardsBeingRemoved <0 || amountOfCardsBeingRemoved > 52){
			System.out.println("Input at the range of 0-52");
			amountOfCardsBeingRemoved = Integer.parseInt(input.nextLine());
		}
		else{
			validAmount = false;
		}
	}
	
	System.out.println("Amount of cards previously: " + deck.length());
	for(int i =0; i < amountOfCardsBeingRemoved; i++){
		deck.drawTopCard();
	}
	
	System.out.println("Amount of cards now: " + deck.length());
	deck.shuffle();
	System.out.println(deck);
	
	/*	Card card = new Card(Card.Suit.Hearts, "Ace");
		System.out.println(card); 
		
		Deck deck =  new Deck();
		System.out.println(deck);
		deck.shuffle();
		System.out.println(deck);
		*/
	}
}